package advprog.acronym.bot.source;

import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Database {
    private static final String path = "./src/main/java/advprog/acronym/bot/source/data.json";

    @SuppressWarnings("unchecked")
    public static void addDataToJSON(String acronym, String expansion) {
        try {

            JSONObject jsonObject = getJSON();

            if (jsonObject.get(acronym) == null) {
                jsonObject.put(acronym, expansion);
            } else {
                jsonObject.remove(acronym);
                jsonObject.put(acronym, expansion);
            }

            FileWriter file = new FileWriter(path);
            file.write(jsonObject.toJSONString());

            file.flush();
            file.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteDataInJSON(String acronym) {
        try {

            JSONObject jsonObject = getJSON();
            jsonObject.remove(acronym);

            FileWriter file = new FileWriter(path);
            file.write(jsonObject.toJSONString());

            file.flush();
            file.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getJSON() {
        JSONParser parser = new JSONParser();
        Object obj = new Object();

        try {
            obj = parser.parse(new FileReader(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return (JSONObject) obj;
    }

    public static Map<String,String> convertJSONtoMap() {
        JSONObject json = getJSON();
        HashMap<String, String> acronyms = new HashMap<String, String>();

        for(Object o : json.keySet()) {
            acronyms.put((String) o, (String) json.get(o));
        }

        return acronyms;
    }
}