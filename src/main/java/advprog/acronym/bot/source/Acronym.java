package advprog.acronym.bot.source;

public class Acronym {
    private String acronym;
    private String expansion;

    public Acronym() {
        acronym = "";
        expansion = "";
    }

    public String getAcronym() {
        return acronym;
    }

    public String getExpansion() {
        return expansion;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public void setExpansion(String expansion) {
        this.expansion = expansion;
    }
}
