package advprog.acronym.bot.controller;

public class ChatState implements State {
    private String state;

    public ChatState() {
        state = "";
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
