package advprog.acronym.bot.controller;

import advprog.acronym.bot.source.Acronym;
import advprog.acronym.bot.source.Database;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.TemplateMessage;
import com.linecorp.bot.model.message.template.CarouselColumn;
import com.linecorp.bot.model.message.template.CarouselTemplate;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

@LineMessageHandler
public class AcronymController {

    private static final Logger LOGGER = Logger.getLogger(AcronymController.class.getName());
    private ChatState cs = new ChatState();
    private Acronym acro = new Acronym();

    @Autowired
    LineMessagingClient lineMessagingClient;

    @EventMapping
    public TextMessage handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        LOGGER.fine(String.format("TextMessageContent(timestamp='%s',content='%s')",
                event.getTimestamp(), event.getMessage()));
        TextMessageContent content = event.getMessage();
        String contentText = content.getText();
        return new TextMessage(replyText(contentText, cs.getState(), event));
    }

    @EventMapping
    public void handleDefaultMessage(Event event) {
        LOGGER.fine(String.format("Event(timestamp='%s',source='%s')",
                event.getTimestamp(), event.getSource()));
    }

    private String replyText(String contentText, String state,
                             MessageEvent<TextMessageContent> event) {
        String result = "";
        Map<String, String> json = Database.convertJSONtoMap();
        return replyTextHelper(result, contentText, state, json, event);
    }

    private String replyTextHelper(String result, String contentText, String state,
                                   Map<String, String> json, MessageEvent<TextMessageContent> event) {
        if (event.getSource() instanceof UserSource) {

            if (cs.getState().equals("")) {
                if (contentText.equals("/add_acronym")) {
                    cs.setState(contentText);
                    result = "Tulis akronim kamu";
                } else if (contentText.equals("/update_acronym")) {
                    cs.setState(contentText);
                    result = "Pilih akronim yang ingin diperbarui";
                    carousel(event, result);
                } else if (contentText.equals("/delete_acronym")) {
                    cs.setState(contentText);
                    result = "Pilih akronim yang ingin dihapus";
                    carousel(event, result);
                }
            } else if (cs.getState().equals("/add_acronym")) {
                cs.setState("add_expansion");
                acro.setAcronym(contentText);
                result = "Tulis kepanjangannya";
            } else if (cs.getState().equals("add_expansion")) {
                cs.setState("");
                acro.setExpansion(contentText);
                Database.addDataToJSON(acro.getAcronym(), acro.getExpansion());
                result = acro.getAcronym() + ": " + acro.getExpansion();
            } else if (cs.getState().equals("/update_acronym")) {
                acro.setAcronym(contentText);
                if (json.containsKey(acro.getAcronym())) {
                    cs.setState("update_expansion");
                    result = "Tulis kepanjangan yang baru (DEBUG: your reply token is "+event.getReplyToken();
                } else {
                    result = "Akronim kamu belum terdaftar, coba lagi";
                }
            } else if (cs.getState().equals("update_expansion")) {
                cs.setState("");
                acro.setExpansion(contentText);
                Database.addDataToJSON(acro.getAcronym(), acro.getExpansion());
                result = acro.getAcronym() + ": " + acro.getExpansion();
            } else if (cs.getState().equals("/delete_acronym")) {
                acro.setAcronym(contentText);
                if (json.containsKey(acro.getAcronym())) {
                    cs.setState("confirmation");
                    result = "Y/N ?";
                } else {
                    result = "Akronim kamu belum terdaftar, coba lagi";
                }
            } else if (cs.getState().equals("confirmation")) {
                cs.setState("");
                Database.deleteDataInJSON(acro.getAcronym());
                result = "Akronim \"" + acro.getAcronym() + "\" berhasil dihapus";
            }
        }
        return result;
    }

    private void reply(String replyToken, TemplateMessage message) {
        try {
            lineMessagingClient.replyMessage(new ReplyMessage(replyToken, message)).get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Error");
        }
    }

    private void carousel(MessageEvent<TextMessageContent> event, String altText) {
        System.out.println(event.getReplyToken());
        try {
            String imageUrl = "http://bandungcitytoday.com/wp-content/" +
                    "uploads/2017/05/unnamed-300x300.jpg";

            Map<String, String> acronyms = Database.convertJSONtoMap();
            PostbackAction[] data = new PostbackAction[acronyms.size()];

            int i = 0;
            for(String key : acronyms.keySet()) {
                data[i] = new PostbackAction(key, acronyms.get(key), key);
            }

            CarouselTemplate carouselTemplate = new CarouselTemplate(
                    Collections.singletonList(
                            new CarouselColumn(imageUrl,
                                    "Acronym", altText, Arrays.asList(data)
                            )
                    ));
            TemplateMessage templateMessage =
                    new TemplateMessage("Carousel alt text", carouselTemplate);
            reply(event.getReplyToken(), templateMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
