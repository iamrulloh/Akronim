package advprog.acronym.bot.controller;

public interface State {
    public String getState();
    public void setState(String state);
}
