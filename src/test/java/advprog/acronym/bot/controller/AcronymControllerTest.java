package advprog.acronym.bot.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import advprog.acronym.bot.EventTestUtil;

import advprog.acronym.bot.source.Database;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(properties = "line.bot.handler.enabled=false")
@ExtendWith(SpringExtension.class)
public class AcronymControllerTest {

    static {
        System.setProperty("line.bot.channelSecret", "SECRET");
        System.setProperty("line.bot.channelToken", "TOKEN");
    }

    @Autowired
    private AcronymController acronymController;

    @Test
    void testContextLoads() {
        assertNotNull(acronymController);
    }

    @Test
    void testHandleTextMessageEventAddAcronymFirst() {
        MessageEvent<TextMessageContent> event =
                EventTestUtil.createDummyTextMessage("/update_acronym");
        TextMessage reply = acronymController.handleTextMessageEvent(event);
        event =
                EventTestUtil.createDummyTextMessage("HAHA");
        reply = acronymController.handleTextMessageEvent(event);
        assertEquals("Akronim kamu belum terdaftar, coba lagi", reply.getText());
    }

    @Test
    void testHandleDefaultMessage() {
        Event event = mock(Event.class);

        acronymController.handleDefaultMessage(event);

        verify(event, atLeastOnce()).getSource();
        verify(event, atLeastOnce()).getTimestamp();
    }
}